﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SOS
{
    public class EMailSetting
    {
       private static readonly Regex sosEx = new Regex("\\s+\"SOSetting\":.*", RegexOptions.Compiled);
       private static readonly Regex emailSettingEx = new Regex("\\s+\"EMailSetting\":.*", RegexOptions.Compiled);
       private static readonly Regex serverEx = new Regex("\\s+\"Server\":\\s+\"?([^,\"]*)\"?,?", RegexOptions.Compiled);
       private static readonly Regex portEx = new Regex("\\s+\"Port\":\\s+\"?([^,\"]*)\"?,?", RegexOptions.Compiled);
       private static readonly Regex userEx = new Regex("\\s+\"User\":\\s+\"?([^,\"]*)\"?,?", RegexOptions.Compiled);
       private static readonly Regex pwdEx = new Regex("\\s+\"Pwd\":\\s+\"?([^,\"]*)\"?,?", RegexOptions.Compiled);
       private static readonly Regex secureEx = new Regex("\\s+\"Secure\":\\s+\"?([^,\"]*)\"?,?", RegexOptions.Compiled);
       private static readonly Regex fromEx = new Regex("\\s+\"From\":\\s+\"?([^,\"]*)\"?,?", RegexOptions.Compiled);
       private static readonly Regex nameEx = new Regex("\\s+\"Name\":\\s+\"?([^,\"]*)\"?,?", RegexOptions.Compiled);
        public EMailSetting()
        {
            string fileName = "appsettings.json";
            try
            {
                string line = null;



                bool hadSOS = false, hadEmailSetting = false, hadServer = false, hadPort = false, hadUser = false, hadPwd = false, hadSecure = false, hadFrom = false, hadName = false;
                using (System.IO.TextReader reader = new StreamReader(fileName))
                {
                    line = reader.ReadLine();
                    while(line != null)
                    {
                        if (!hadSOS) { if (sosEx.IsMatch(line)) hadSOS = true; }
                        else
                        {
                            if (!hadEmailSetting) { if (emailSettingEx.IsMatch(line)) hadEmailSetting = true; }
                            else
                            {
                                if (!hadServer) if (serverEx.IsMatch(line)) { hadServer = true; this.Server = serverEx.Matches(line)[0].Groups[1].Value; }
                                if (!hadPort) if (portEx.IsMatch(line)) { hadPort = true; this.Port = portEx.Matches(line)[0].Groups[1].Value; }
                                if (!hadUser) if (userEx.IsMatch(line)) { hadUser = true; this.User = userEx.Matches(line)[0].Groups[1].Value; }
                                if (!hadPwd) if (pwdEx.IsMatch(line)) { hadPwd = true; this.Pwd = pwdEx.Matches(line)[0].Groups[1].Value; }
                                if (!hadSecure) if (secureEx.IsMatch(line)) { hadSecure = true; this.Secure = Convert.ToBoolean(secureEx.Matches(line)[0].Groups[1].Value); }
                                if (!hadFrom) if (fromEx.IsMatch(line)) { hadFrom = true; this.From = fromEx.Matches(line)[0].Groups[1].Value; }
                                if (!hadName) if (nameEx.IsMatch(line)) { hadName = true; this.Name = nameEx.Matches(line)[0].Groups[1].Value; }
                                if (hadServer && hadPort && hadUser && hadPwd && hadSecure && hadFrom && hadName) break;
                            }

                        }
                        line = reader.ReadLine();
                    }
                }
            }catch (Exception ex)
            {
                //unable to set values from appsettings...
            }
        }
        public string Server { get; set; }
        public string Port { get; set; }
        public string User { get; set; }
        public string Pwd { get; set; }
        public bool Secure { get; set; }
        public string From { get; set; }
        public string Name { get; set; }
    }

    public class SMSSetting
    {
      private static readonly  Regex sosEx = new Regex("\\s+\"SOSetting\":.*", RegexOptions.Compiled);
      private static readonly  Regex sMSSettingEx = new Regex("\\s+\"SMSSetting\":.*", RegexOptions.Compiled);
      private static readonly  Regex gateEx = new Regex("\\s+\"Gate\":\\s+\"?([^,\"]*)\"?,?", RegexOptions.Compiled);
      private static readonly  Regex userEx = new Regex("\\s+\"User\":\\s+\"?([^,\"]*)\"?,?", RegexOptions.Compiled);
      private static readonly  Regex pwdEx = new Regex("\\s+\"Pwd\":\\s+\"?([^,\"]*)\"?,?", RegexOptions.Compiled);
      private static readonly  Regex senderEx = new Regex("\\s+\"Sender\":\\s+\"?([^,\"]*)\"?,?", RegexOptions.Compiled);
        public SMSSetting()
        {
            string fileName = "appsettings.json";
            try
            {
                string line = null;



                bool hadSOS = false, hadSMSSetting = false, hadGate = false, hadUser = false, hadPwd = false, hadSender = false;
                using (System.IO.TextReader reader = new StreamReader(fileName))
                {
                    line = reader.ReadLine();
                    while (line != null)
                    {
                        if (!hadSOS) { if (sosEx.IsMatch(line)) hadSOS = true; }
                        else
                        {
                            if (!hadSMSSetting) { if (sMSSettingEx.IsMatch(line)) hadSMSSetting = true; }
                            else
                            {
                                if (!hadGate) if (gateEx.IsMatch(line)) { hadGate = true; this.Gate = gateEx.Matches(line)[0].Groups[1].Value; }
                                if (!hadUser) if (userEx.IsMatch(line)) { hadUser = true; this.User = userEx.Matches(line)[0].Groups[1].Value; }
                                if (!hadPwd) if (pwdEx.IsMatch(line)) { hadPwd = true; this.Pwd = pwdEx.Matches(line)[0].Groups[1].Value; }
                                if (!hadSender) if (senderEx.IsMatch(line)) { hadSender = true; this.Sender = senderEx.Matches(line)[0].Groups[1].Value; }
                                if (hadGate &&  hadUser && hadPwd && hadSender) break;
                            }

                        }
                        line = reader.ReadLine();
                    }
                }
            }
            catch (Exception ex)
            {
                //unable to set values from appsettings...
            }
        }
        public string Gate { get; set; }
        public string User { get; set; }
        public string Pwd { get; set; }
        public string Sender { get; set; }
    }

    public class ChatSetting
    {
       private static readonly Regex sosEx = new Regex("\\s+\"SOSetting\":.*", RegexOptions.Compiled);
       private static readonly Regex chatSettingEx = new Regex("\\s+\"ChatSetting\":.*", RegexOptions.Compiled);
       private static readonly Regex gateEx = new Regex("\\s+\"Gate\":\\s+\"?([^,\"]*)\"?,?", RegexOptions.Compiled);
       private static readonly Regex instanceEx = new Regex("\\s+\"Instance\":\\s+\"?([^,\"]*)\"?,?", RegexOptions.Compiled);
        public ChatSetting()
        {
            string fileName = "appsettings.json";
            try
            {
                string line = null;



                bool hadSOS = false, hadChatSetting = false, hadGate = false, hadInstance = false;
                using (System.IO.TextReader reader = new StreamReader(fileName))
                {
                    line = reader.ReadLine();
                    while (line != null)
                    {
                        if (!hadSOS) { if (sosEx.IsMatch(line)) hadSOS = true; }
                        else
                        {
                            if (!hadChatSetting) { if (chatSettingEx.IsMatch(line)) hadChatSetting = true; }
                            else
                            {
                                if (!hadGate) if (gateEx.IsMatch(line)) { hadGate = true; this.Gate = gateEx.Matches(line)[0].Groups[1].Value; }
                                if (!hadInstance) if (instanceEx.IsMatch(line)) { hadInstance = true; this.Instance = instanceEx.Matches(line)[0].Groups[1].Value; }
                                if (hadGate && hadInstance) break;
                            }

                        }
                        line = reader.ReadLine();
                    }
                }
            }
            catch (Exception ex)
            {
                //unable to set values from appsettings...
            }
        }
        public string Gate { get; set; }
        public string Instance { get; set; }
    }
}
